Rails.application.routes.draw do
  get "/", to: "welcome#index"
  
  get "/welcome-contacts", to: "welcome#contacts"

  get "/welcome-help", to: "welcome#help"

  get "/welcome-share", to: "welcome#share"

  get "/pages-grid", to: "pages#grid"

  get "/pages-list", to: "pages#list"

  get "/pages-single", to: "pages#single"

  get "/session-register", to: "session#register"

  get "/session-forgot", to: "session#forgot"

  get "/session-login", to: "session#login"

  get "/agents-grid", to: "agents#grid"

  get "/agents-list", to: "agents#list"

  get "/agents-single", to: "agents#single"
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "welcome#index"
end
