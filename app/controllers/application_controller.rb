# ApplicationController
#
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :touch_cdn

  CDNJS_ASSETS = {
    jquery: '//cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
    migrate: '//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.1/'\
             'jquery-migrate.min.js',
    popper: '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/popper.min.js',
    bootstrap: '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/'\
               'bootstrap.min.js',
    chosen: '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/'\
            'chosen.jquery.min.js',
    slick: '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js',
    fancybox: '//cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/'\
              'jquery.fancybox.min.js'
  }

  CDNCSS_ASSETS = {
    bootstrap: '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/'\
               'bootstrap.min.css',
    font_awesome: '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/'\
                  'font-awesome.min.css',
    simple_line_icons: '//cdnjs.cloudflare.com/ajax/libs/simple-line-icons/'\
                       '2.4.1/css/simple-line-icons.min.css',
    animate: '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/'\
             'animate.min.css',
    hamburgers: '//cdnjs.cloudflare.com/ajax/libs/hamburgers/0.9.3/'\
                'hamburgers.min.css',
    chosen: '//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.7/chosen.min.css',
    slick_carousel: '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/'\
                    'slick.min.css',
    fancybox: '//cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/'\
              'jquery.fancybox.min.css'
  }

  def touch_cdn
    begin
      require 'net/http'
      uri = URI('https://cdnjs.com')
      response = Net::HTTP.get_response(uri)
      @jsassets = localjs
      @jsassets = CDNJS_ASSETS if response.message == 'OK'
      @cssassets = localcss
      @cssassets = CDNCSS_ASSETS if response.message == 'OK'
    rescue
      @jsassets = localjs
      @cssassets = localcss
    end
  end

  def localcss
    data = {}
    data[:bootstrap] = "#{my_domain}/assets/vendor/bootstrap/bootstrap.min.css"
    data[:font_awesome] = "#{my_domain}/assets/vendor/icon-awesome/css/font-awesome.min.css"
    data[:simple_line_icons] = "#{my_domain}/assets/vendor/icon-line/css/simple-line-icons.css"
    data[:animate] = "#{my_domain}/assets/vendor/animate.css"
    data[:hamburgers] = "#{my_domain}/assets/vendor/hamburgers/hamburgers.min.css"
    data[:chosen] = "#{my_domain}/assets/vendor/chosen/chosen.css"
    data[:slick_carousel] = "#{my_domain}/assets/vendor/slick-carousel/slick/slick.css"
    data[:fancybox] = "#{my_domain}/assets/vendor/fancybox/jquery.fancybox.min.css"
    data
  end

  def localjs
    data = {}
    data[:jquery] = "#{my_domain}/assets/vendor/jquery/jquery.min.js"
    data[:migrate] = "#{my_domain}/assets/vendor/jquery-migrate/jquery-migrate.min.js"
    data[:popper] = "#{my_domain}/assets/vendor/popper.min.js"
    data[:bootstrap] = "#{my_domain}/assets/vendor/bootstrap/bootstrap.min.js"
    data[:chosen] = "#{my_domain}/assets/vendor/chosen/chosen.jquery.js"
    data[:slick] = "#{my_domain}/assets/vendor/slick-carousel/slick/slick.js"
    data[:fancybox] = "#{my_domain}/assets/vendor/fancybox/jquery.fancybox.min.js"
    data
  end

  def my_domain
    domain = ''
    domain = request.protocol
    domain += request.domain
    domain += ":#{request.port.to_s}" if request.domain == 'localhost'
    domain
  end
end
